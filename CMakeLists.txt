cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME driverRotorsSimulatorROSModule)
project(${PROJECT_NAME})

### Use version 2011 of C++ (c++11). By default ROS uses c++98
#see: http://stackoverflow.com/questions/10851247/how-to-activate-c-11-in-cmake
#see: http://stackoverflow.com/questions/10984442/how-to-detect-c11-support-of-a-compiler-with-cmake

#add_definitions(-std=c++0x)
#add_definitions(-std=c++0x)
#add_definitions(-std=c++03)
add_definitions(-std=c++11)


# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries
#set(ROS_BUILD_TYPE RelWithDebInfo)



set(DRIVER_ROTORS_SIMULATOR_SOURCE_DIR
	src/source)
	
set(DRIVER_ROTORS_SIMULATOR_INCLUDE_DIR
	src/include
	)

set(DRIVER_ROTORS_SIMULATOR_SOURCE_FILES
        src/source/rotorsSimulatorDroneOuts.cpp
        src/source/rotorsSimulatorDroneInps.cpp
	)
	
set(DRIVER_ROTORS_SIMULATOR_HEADER_FILES
        src/include/rotorsSimulatorDroneOuts.h
        src/include/rotorsSimulatorDroneInps.h
	)

find_package(catkin REQUIRED
                COMPONENTS roscpp std_msgs geometry_msgs sensor_msgs gazebo_msgs mavros droneModuleROS droneMsgsROS lib_cvgutils tf_conversions)



catkin_package(
#       INCLUDE_DIRS ${OKTODRIVER_INCLUDE_DIR}
        CATKIN_DEPENDS roscpp std_msgs geometry_msgs sensor_msgs gazebo_msgs mavros droneModuleROS droneMsgsROS lib_cvgutils tf_conversions
  )


include_directories(${DRIVER_ROTORS_SIMULATOR_INCLUDE_DIR})
include_directories(${catkin_INCLUDE_DIRS})


add_library(driverRotorsSimulatorROSModule ${DRIVER_ROTORS_SIMULATOR_SOURCE_FILES} ${DRIVER_ROTORS_SIMULATOR_HEADER_FILES})
add_dependencies(driverRotorsSimulatorROSModule ${catkin_EXPORTED_TARGETS})
target_link_libraries(driverRotorsSimulatorROSModule ${catkin_LIBRARIES})


add_executable(rotationAnglesRotorsSimulatorROSModule src/source/rotorsSimulatorRotationAnglesNode.cpp)
add_dependencies(rotationAnglesRotorsSimulatorROSModule ${catkin_EXPORTED_TARGETS})
target_link_libraries(rotationAnglesRotorsSimulatorROSModule driverRotorsSimulatorROSModule)
target_link_libraries(rotationAnglesRotorsSimulatorROSModule ${catkin_LIBRARIES})

add_executable(droneCommandRotorsSimulatorROSModule  src/source/rotorsSimulatorDroneCommandNode.cpp)
add_dependencies(droneCommandRotorsSimulatorROSModule ${catkin_EXPORTED_TARGETS})
target_link_libraries(droneCommandRotorsSimulatorROSModule driverRotorsSimulatorROSModule)
target_link_libraries(droneCommandRotorsSimulatorROSModule ${catkin_LIBRARIES})

add_executable(droneAltitudeRotorsSimulatorROSModule  src/source/rotorsSimulatorDroneAltitudeNode.cpp)
add_dependencies(droneAltitudeRotorsSimulatorROSModule ${catkin_EXPORTED_TARGETS})
target_link_libraries(droneAltitudeRotorsSimulatorROSModule driverRotorsSimulatorROSModule)
target_link_libraries(droneAltitudeRotorsSimulatorROSModule ${catkin_LIBRARIES})

add_executable(droneLocalPoseRotorsSimulatorROSModule  src/source/rotorsSimulatorDroneLocalPoseNode.cpp)
add_dependencies(droneLocalPoseRotorsSimulatorROSModule ${catkin_EXPORTED_TARGETS})
target_link_libraries(droneLocalPoseRotorsSimulatorROSModule driverRotorsSimulatorROSModule)
target_link_libraries(droneLocalPoseRotorsSimulatorROSModule ${catkin_LIBRARIES})


